<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';
    protected $fillable = ['name', 'type_status_id'];
    protected $guarded = ['id'];   

    //Relacion Muchos a Uno de Status a TypeStatus 
    public function typeStatus()
    {
    	return $this->belongsTo('App\Models\TypeStatus');
    }

    //Relacion Uno a muchos de Status y Category
    public function categories()
    {
    	return $this->hasMany('App\Models\Category');
    }
}
