<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Result;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function app()
    {
        $baseUrl = config('app.url');
        return view('roles.app', compact('baseUrl'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {            
            $roles = Role::where('name','LIKE','%'.$request->name.'%')->get();
            return response()->json(Result::success($roles->toArray()));    
        } catch(\Exception $e) {
            return response()->json(Result::error('Error,'.$e->getMessage())); 
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $rol = new Role;
            $rol->name = $request->name;
            $rol->status_id = 1;
            $rol->save();

            if(!isset($rol->id))
                return response()->json(Result::error('Error guardando El Rol.')); 


            return response()->json(Result::success([], 'Rol Guardado Correctamente.'));    
        } catch(\Exception $e) {
            return response()->json(Result::error('Error,'.$e->getMessage())); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {            
            $rol = Role::find($id);

            if(!isset($rol->id))
                return response()->json(Result::error('Error El Rol no Existe.')); 


            return response()->json(Result::success(['rol' => $rol]));    
        } catch(\Exception $e) {
            return response()->json(Result::error('Error,'.$e->getMessage())); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {            
            $rol = Role::find($id);
            
            if(!isset($rol->id))
                return response()->json(Result::error('Error El Rol no Existe.')); 
            
                $rol->name = $request->name;
                $rol->status_id = $request->status_id;
                $rol->save();

            return response()->json(Result::success([], 'Rol Editado Correctamente.'));    
        } catch(\Exception $e) {
            return response()->json(Result::error('Error,'.$e->getMessage())); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {            
            $rol = Role::find($id);
            
            if(!isset($rol->id))
                return response()->json(Result::error('Error El Rol no Existe.')); 
                       
            $rol->delete();

            return response()->json(Result::success([], 'Rol Eliminado Correctamente.'));
        } catch(\Exception $e) {
            return response()->json(Result::error('Error,'.$e->getMessage())); 
        }    
    }
}
